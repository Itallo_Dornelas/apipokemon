import { useState } from "react";
import { Container, Img } from "./styles";
function App() {
  const [dados, setDados] = useState({ name: "" });
  const [search, setSearch] = useState("");
  const getDados = () => {
    const url = `https://pokeapi.co/api/v2/pokemon/${search}`;
    fetch(url, {
      method: "get",
    }).then(function (response) {
      response.json().then(function (data) {
        setDados(data);
      });
    });
  };

  return (
    <Container>
      <h1>{dados.name}</h1>
      <Img
        src={dados?.sprites?.other?.dream_world?.front_default}
        alt={dados.name}
      />
      <input
        type="text"
        placeholder="Escolha o Pokemon"
        onChange={(e) => {
          setSearch(e.target.value);
        }}
      />
      <button onClick={getDados}>Clicar</button>
    </Container>
  );
}

export default App;
